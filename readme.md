## Usage:

`java -jar path\to\jar\AnnotationInjecter-<version>.jar -s <path to sources> -d <path to output directory> [--no-annotations | --no-serializable | --no-fields-grouping]
        --no-annotations: avoid 'javax.xml.bind.annotation' annotations in reproduced classes
        --no-serializable: reproduced classes are WON'T be serializable
        --no-fields-grouping: the fields of the classes reproduced will not be listed together at the beginning of the body but you will have: 1) declaration of the field, 2) setter, 3) getter`