package mbl.tools.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import mbl.loaders.ConsoleLoader;

public class App {
	private static boolean noAnnotations = false;
	private static boolean noSerializable = false;
	private static boolean noFieldsGrouping = false;
	
	private static String sourcePath = null, destPath = null;
	
	private static HashMap<String, String> annotsToReplace = null;

	public static void main(String[] args) {
		for(int i = 0;i < args.length;i++) {
			String arg = args[i];
			
			switch (arg.toUpperCase()) {
				case "-S":
					if((i+1) < args.length && !args[(i+1)].startsWith("-")) {
						sourcePath = args[++i];
					}
					break;
				case "-D":
					if((i+1) < args.length && !args[(i+1)].startsWith("-")) {
						destPath = args[++i];
					}
					break;
				case "--NO-ANNOTATIONS":
					noAnnotations = true;
					break;	
				case "--NO-SERIALIZABLE":
					noSerializable = true;
					break;	
				case "--NO-FIELDS-GROUPING":
					noFieldsGrouping = true;
					break;	
				case "--ANNOTATION-REPLACING":
					annotsToReplace = new HashMap<String, String>();
					for(;i < args.length;i++) {
						String[] anno = args[i].split("[,]");
						if(anno.length >= 2) {
							annotsToReplace.put(anno[0], anno[1]);
						}
					}
					break;	
				default:
					break;
			}
		}
		
		if(sourcePath != null && destPath != null) {
			System.out.println("Loading classes from:" + System.lineSeparator() + "\t" + sourcePath);
			
			System.out.println("Writing parsed classes into:" + System.lineSeparator() + "\t" + destPath);
			
			File dirSource = new File(sourcePath), dirDest = new File(destPath);
			Stack<File> dirs = new Stack<File>();
			ArrayList<File> classesToParse = new ArrayList<File>();
			
			if(dirSource.isFile()) {
				classesToParse.add(dirSource);
			}
			else {
				do {
					for(File f : dirSource.listFiles()) {
						if(f.isFile()) {
							String name = f.getName();
							if(!name.startsWith("_"))
								classesToParse.add(f);
						}
						else {
							dirs.push(f);
						}
					}
					
					if(!dirs.isEmpty())
						dirSource = dirs.pop();
					
				}while(!dirs.isEmpty());
			}
			
			if(!dirDest.exists())
				dirDest.mkdirs();
			else {
				if(!dirDest.getAbsoluteFile().isDirectory())
					dirDest = dirDest.getAbsoluteFile().getParentFile(); 
			}
			
			if(!classesToParse.isEmpty()) {
				ArrayList<Thread>parsers = new ArrayList<Thread>();
				
				ConsoleLoader cl = new ConsoleLoader(classesToParse.size());
				
				for(File classToParse : classesToParse) {
					Thread t = new Thread(new Parser(classToParse, dirDest));
					t.start();
					
					parsers.add(t);
				}
				
				int n = 0;
				System.out.print(cl.console_loader(n++, true) + "\r");
				for(Thread p : parsers) {
					try {
						p.join();
					}
					catch (InterruptedException e) {}
					
					System.out.print(cl.console_loader(n++, true) + "\r");
				}
			}
		}
		else {
			System.out.println("Usage:" + System.lineSeparator() 
							+ "\tjava -jar path" + File.separator + "to" + File.separator + "jar" + File.separator + "AnnotationInjecter-<version>.jar "
							+ "-s <path to sources> "
							+ "-d <path to output directory> "
							+ "[--no-annotations | --no-serializable | --no-fields-grouping | --annotation-replacing {<annotation1>,<replace1> <annotation2>,<replace2> ...}]" + System.lineSeparator()
							+ "\t--no-annotations: avoid 'javax.xml.bind.annotation' annotations in reproduced classes" + System.lineSeparator()
							+ "\t--no-serializable: reproduced classes are WON'T be serializable " + System.lineSeparator()
							+ "\t--no-fields-grouping: the fields of the classes reproduced will not be listed together at the beginning of the body but you will have: 1) declaration of the field, 2) setter, 3) getter" + System.lineSeparator()
							+ "\t--annotation-replacing: enable the (textual) annotation replacing, using the provided list of annotation \"to look for\" and annotation \"to put\". WARNING any necessary imports must be done MANUALLY");
		}
	}
	
	public static String getDestPath() {
		return destPath;
	}

	public static boolean getNoAnnotations() {
		return noAnnotations;
	}

	public static boolean getNoSerializable() {
		return noSerializable;
	}

	public static boolean getNoFieldsGrouping() {
		return noFieldsGrouping;
	}

	public static HashMap<String, String> getAnnotsToReplace() {
		return annotsToReplace;
	}
}
