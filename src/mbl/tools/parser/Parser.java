package mbl.tools.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;

public class Parser implements Runnable{
	private JavaParser parser = null;
	
	private File classToParseRef = null;
	private File destIntoParse = null;
	
	private ArrayList<File> staticClasses = null;
	
	private BufferedWriter bw = null;
	
	public Parser(File classToParse, File dirDest) {
		this.classToParseRef = classToParse;
		this.destIntoParse = dirDest;
		this.staticClasses = new ArrayList<File>();
		
		parser = new JavaParser();
	}
	
	public void run() {
		try {
			String className = classToParseRef.getName().split("[.]")[0];
			
			ClassOrInterfaceDeclaration classDeclaration = classAnalize(classToParseRef, className);
			
			if(classDeclaration != null && classDeclaration.findCompilationUnit().isPresent()) {
				String parsedPath = destIntoParse.getAbsolutePath();
				CompilationUnit com = classDeclaration.findCompilationUnit().get();
				PackageDeclaration pkg = null;
				String[] pkgTkns = null;
				
				
				if(com.getPackageDeclaration().isPresent()) {
					pkg = com.getPackageDeclaration().get();
					
					pkgTkns = pkg.getName().asString().split("[.]");
				}
				
				/* Riproduco il package come struttura di cartelle */
				for(String tkn : pkgTkns) {
					parsedPath += File.separator + tkn;
				}
				
				/* Creo il file per il sorgente "annotato" */
				File parsed = new File(parsedPath + File.separator + className + ".java");
				if(!parsed.getParentFile().exists())
					parsed.getParentFile().mkdirs();
				
				bw = new BufferedWriter(new FileWriter(parsed));
				
				/* Riporto il package originale */
				if(pkg != null) {
					bw.write("package " + pkg.getNameAsString() + ";");
					bw.newLine();
					bw.newLine();
				}
				
				/* Inizio la trascrizione */
				transcribeClass(classDeclaration, com, "");
				
				if(bw != null)
					bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Logica per la trascrizione del sorgente di una classe JF, nella corrispondente classe con annotazioni XML
	 * @param classDeclaration
	 * @param com
	 * @param indent
	 * @throws IOException
	 */
	private void transcribeClass(ClassOrInterfaceDeclaration classDeclaration, CompilationUnit com, String indent) throws IOException {
		boolean serializable = false;
		for(ClassOrInterfaceType type : classDeclaration.getImplementedTypes())
			serializable |= type.getNameAsString().contains("Serializable");
		
		/* Riporto gli import */
		if(com != null) {
			for(ImportDeclaration imp : com.getImports()) {
				bw.write("import " + imp.getNameAsString() + ";");
				bw.newLine();
			}
			
			if(!serializable && !App.getNoSerializable()) {
				bw.write("import java.io.Serializable;");
				bw.newLine();
			}
			
			if(!App.getNoAnnotations()) {
				bw.write("import javax.xml.bind.annotation.XmlElement;");
				bw.newLine();
				bw.write("import javax.xml.bind.annotation.XmlType;");
				bw.newLine();
			}
			
			bw.newLine();
		}
		
		/* Recupero le righe del sorgente, per prendere la segnatura della classe */
		String[] rows = classDeclaration.toString().split(System.lineSeparator());
		int i = 0;
		for(;i < rows.length;i++) {
			if(rows[i].contains("class "))
				break;
		}
		/* Annoto la segnatura della classe ed aggiungo l'implementazione di Serilizable se non presente*/
		classAnnotation(indent, classDeclaration.getNameAsString(), serializable, rows[i], bw);
		
		/* Annoto tutti i campi e per ognuno ricreo setter e getter*/
		fieldsAnnotation(indent + "\t", classDeclaration.getFields(), bw);
		
		bw.flush();
		
		/* Ricerco ed elaboro eventuali classi innestate */
		classDeclaration.getMembers().stream().filter(m -> m.isClassOrInterfaceDeclaration()).forEach(member -> {
			ClassOrInterfaceDeclaration subclass = (ClassOrInterfaceDeclaration)member;
			
			try {
				transcribeClass(subclass, null, "\t");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		bw.write(indent + "}");
		bw.newLine();
		bw.flush();
	}
	
	/**
	 * Logica che analizza il sorgente nel .java e me ne restituisce un "esploso" 
	 * @param classRef
	 * @param className
	 * @return
	 * @throws FileNotFoundException
	 */
	private ClassOrInterfaceDeclaration classAnalize(File classRef, String className) throws FileNotFoundException {		
		ClassOrInterfaceDeclaration out = null;
		ParseResult<CompilationUnit> res = parser.parse(classRef);
		
		if(res.getResult().isPresent() && res.getResult().get().getClassByName(className).isPresent())
			out = res.getResult().get().getClassByName(className).get();
		
		return out;
	}

	private void classAnnotation(String ws, String name, boolean serializable, String line, BufferedWriter bw) throws IOException {		
		if(!App.getNoAnnotations()) {
			bw.write(ws + "@XmlType (name = \"" + name + "\")");
			bw.newLine();
		}
		
		if(!serializable && !App.getNoSerializable()) {
			line = line.replace("{", "");						
			if(line.contains("implements"))
				line += ", Serializable";
			else
				line += "implements Serializable";
			
			line += "{";
		}
		
		bw.write(ws + line);
		bw.newLine();
	}

	private void fieldsAnnotation(String ws, List<FieldDeclaration> fields, BufferedWriter bw) throws IOException {
		if(App.getNoFieldsGrouping()) {
			for(FieldDeclaration field : fields) {
				String type = field.getElementType().asString();
				NodeList<VariableDeclarator> variables = field.getVariables();

				annotationReplacing(bw, ws, field);
				
				for(VariableDeclarator var : variables) {
					String fieldName = var.getNameAsString();
					
					if(!App.getNoAnnotations()) {
						// inserisco l'annotazione
						bw.write(ws + "@XmlElement (name = \"" + fieldName + "\")");
						bw.newLine();
					}
					
					// scrivo la dichiarazione indentata del singolo campo
					bw.write(ws + "private " + type + " " + var.toString() + ";");
					bw.newLine();
					bw.newLine();
					
					// Ricreo il setter
					buildSetter(bw, ws, fieldName, type);
					
					// Ricreo il getter
					buildGetter(bw, ws, fieldName, type);
				}
			}
		}
		else {
			for(FieldDeclaration field : fields) {
				String type = field.getElementType().asString();
				NodeList<VariableDeclarator> variables = field.getVariables();

				annotationReplacing(bw, ws, field);
				
				for(VariableDeclarator var : variables) {
					String fieldName = var.getNameAsString();
					
					if(!App.getNoAnnotations()) {
						// inserisco l'annotazione
						bw.write(ws + "@XmlElement (name = \"" + fieldName + "\")");
						bw.newLine();
					}
					
					// scrivo la dichiarazione indentata del singolo campo
					bw.write(ws + "private " + type + " " + var.toString() + ";");
					bw.newLine();
				}
			}
			
			bw.newLine();
			bw.flush();
			
			for(FieldDeclaration field : fields) {
				String type = field.getElementType().asString();
				NodeList<VariableDeclarator> variables = field.getVariables();
				
				
				for(VariableDeclarator var : variables) {
					String fieldName = var.getNameAsString();
					
					// Ricreo il setter
					buildSetter(bw, ws, fieldName, type);
					
					// Ricreo il getter
					buildGetter(bw, ws, fieldName, type);
				}
			}
		}
	}
	
	private void annotationReplacing(BufferedWriter bw, String ws, FieldDeclaration field) throws IOException {
		if(App.getAnnotsToReplace() != null && !App.getAnnotsToReplace().isEmpty()) {
			for(AnnotationExpr a : field.getAnnotations()) {
				String newName = App.getAnnotsToReplace().get(a.getName().asString());
				if(newName != null) {
					String annotation = a.toString();
					bw.write(ws + annotation.replace(a.getName().asString(), newName));
					bw.newLine();
				}
			}
		}
	}
	
	private void buildSetter(BufferedWriter bw, String ws, String fieldName, String type) throws IOException {
		bw.write(ws + "public void set" + firstUpper(fieldName) + " (" + type + " " + fieldName + ") {");
		bw.newLine();
		bw.write(ws + "\tthis." + fieldName + " = " + fieldName + ";");
		bw.newLine();
		bw.write(ws + "}");
		bw.newLine();
		bw.newLine();
	}
	
	private void buildGetter(BufferedWriter bw, String ws, String fieldName, String type) throws IOException {
		bw.write(ws + "public " + type + " get" + firstUpper(fieldName) + " () {");
		bw.newLine();
		bw.write(ws + "\treturn this." + fieldName + ";");
		bw.newLine();
		bw.write(ws + "}");
		bw.newLine();
		bw.newLine();
	}

	private String firstUpper(String name) {
		name = name.trim();
		
		if(name.length() > 0 && name.charAt(0) >= 97)
			return ((char)(name.charAt(0) - 32)) + name.substring(1);
		else
			return name;
	}
}
